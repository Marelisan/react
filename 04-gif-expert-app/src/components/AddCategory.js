import React, {useState} from 'react';
import PropTypes from 'prop-types';

export const AddCategory = ({setCategories}) => {
    const [inputValue, setInputValue] = useState('');
    const handleInputChange =(e)=>{
        setInputValue(e.target.value); // inputValue actualizado con la info del input
    };
    const handleSubmit=(e)=>{
        e.preventDefault();
        if(inputValue.trim().length>2){
            setCategories(cats => [inputValue,...cats]);
            setInputValue('');// borra el valor para no hacer docle posteo
        }


    }
    return (
        <form onSubmit ={handleSubmit}>

            <input
                type="text"
                value = {inputValue}
                onChange = {handleInputChange}//llama a la función handleInputChange cada vez que la caja de texto cambie
            />
        </form>
    )
}

AddCategory.propTypes={
    setCategories: PropTypes.func.isRequired,
}