import React from 'react';
import ReactDOM from 'react-dom';
//import PrimeraApp from './PrimeraApp';
import './index.css';
import CounterApp from './CounterApp';


//const saludo = <h1>Hola Mundo</h1>
//const divRoot = document.querySelector('#root'); // referecnia al div root de index html
//Para renderizar la etiqueta h1 dentro del div root 
//ReactDOM.render(saludo,divRoot); // tiene 2 argumentos ,1 lo que quiero mostrar y 2 el componente donde lo quiero mostrar
//Se podría hacer así tambien:
//document.body.append(saludo);
// react dom también nos ofrece otras características, entre las cuales nos va a permitir crear nuestro árbol de componentes
//y eso a su vez nos permite comunicarnos entre componentes de una manera sencilla.

const divRoot = document.querySelector('#root');
//ReactDOM.render(<PrimeraApp saludo='Hola soy un robot'/>,divRoot);
ReactDOM.render(<CounterApp />,divRoot); // es estandar dejar el espacio 
