// Tarea

// 1. Crear un nuevo componente dentro de la carpeta SRC llamado
//     ```CounterApp```

// 2. El CounterApp debe de ser un __Functional Component__

// 3. El contenido del __CounterApp__ debe de ser:
//     ```
//         <h1>CounterApp</h1>
//         <h2> { value } </h2>
//     ```

// 4. Donde ```"value"``` es una propiedad enviada desde el padre hacia
//     el componente __CounterApp__ (Debe ser númerica validada con PropTypes)

// 5. Reemplazar en el index.js el componente de <PrimeraApp />
//     por el componente <CounterApp /> 
//         (no se olviden del value que debe de ser un número)

// 6. Asegúrense de no tener errores ni warnings
//     (Cualquier warning no usado, comentar el código)

import React, {useState} from 'react'
import PropTypes from 'prop-types';



const CounterApp = ({value = 10})=>{

    const[counter,setCounter] = useState(value);

    const handleAdd = ()=>{
        //setCounter(counter+1); //counter++ estamos intentando cambiar el valor de una constante, usemos setCounter
        //setCounter solo renderiza lo que realmente cambia
        setCounter((c)=>c+1); // por si desconocemos el valor del useState.
    }
    const handleReset= ()=> setCounter(value); 
    

    const handleSubstract = ()=> setCounter(counter-1); 
    

    return (
        <>
            <h1>CounterApp</h1>
            <h2> { counter } </h2>
            <button onClick = {handleAdd}>+1</button>
            <button onClick = {handleReset}>Reset</button>
            <button onClick = {handleSubstract}>-1</button>
        </>
    )
}

CounterApp.propTypes={
    value: PropTypes.number
}

export default CounterApp;
