import { getHeroeById, getHeroesByOwner } from "../../base/08-imp-exp"
import heroes from '../../data/heroes';

describe('Pruebas en funciones de Héroes', () => {
    test('Debe retornar un héroe por id', () => {
        const id = 1;
        const heroe = getHeroeById(id);
        const heroeData = heroes.find(hData => hData.id ===id);
        expect (heroe).toEqual(heroeData);
    })

    test('Debe retornar undefined si héroe no existe', () => {
        const id = 10;
        const heroe = getHeroeById(id);
        expect (heroe).toBe(undefined);
    })

    test('Debe de retornar un arreglo con los héroes de DC',()=>{
        const owner = 'DC';
        const heroesArrayData = heroes.filter(hData => hData.owner === owner);
        expect(getHeroesByOwner(owner)).toEqual(heroesArrayData);
    })

    test('Debe de retornar un arreglo con los héroes de Marvel',()=>{
        const owner = 'Marvel';
        //console.log(getHeroesByOwner(owner).length);
        expect(getHeroesByOwner(owner).length).toBe(2);
    })
    
})
