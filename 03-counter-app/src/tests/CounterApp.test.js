import React from "react";
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import CounterApp from "../CounterApp";

describe('Pruebas en <CounterApp />', () => {
    let  wrapper = shallow(<CounterApp />); //si no lo igualamos a esto, no tenemos las ayudas 

    beforeEach(()=>{ // esto se ejecuta antes de cada prueba, ya que al ser en orden los valores de botones cambian
        wrapper = shallow(<CounterApp />);
    });
  
    test('debe de mostrar <CounterApp /> correctamente', ()=>{
        expect(wrapper).toMatchSnapshot();
    });

    test('debe de mostrar el valor por defecto de 100', ()=>{
        //const value = 100;
        const wrapper = shallow(<CounterApp value={100}/>); 
        const counterText = wrapper.find('h2').text().trim();//parecido con el query selector, busca un párrafo para id #id
        //console.log(counterText);
        expect(counterText).toBe('100');
    });

    test('debe de incrementar con el botón +1', ()=>{
        wrapper.find('button').at(0).simulate('click');
        const counterText = wrapper.find('h2').text().trim();
        expect (counterText).toBe('11');
        //const btn1 = wrapper.find('button').at(0).simulate('click');
        //console.log(btn1.html());
    });

    test('debe de decrementar con el botón -1', ()=>{
        wrapper.find('button').at(2).simulate('click');
        const counterText = wrapper.find('h2').text().trim();
        console.log(counterText);
        expect (counterText).toBe('9');
    });

    test('debe de colocar el valor por defecto con el btn reset', ()=>{
        const wrapper = shallow(<CounterApp value={105}/>); 
        wrapper.find('button').at(0).simulate('click'); // simula click en ese boton +1
        wrapper.find('button').at(1).simulate('click');// simula presionar boton reset
        const counterText = wrapper.find('h2').text().trim();
        expect (counterText).toBe('105');
    });
    
});
