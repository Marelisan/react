import React from "react";
//import { render } from "@testing-library/react";
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import PrimeraApp from "../PrimeraApp";

describe('Pruebas en <PrimeraApp />', () => {
    // test('debe de mostrar el mensaje "Hola soy un robot" ', () => {
    //     const saludo = 'Hola soy un robot'; // mensaje que voy a evaluar
    //     //const wrapper = render(<PrimeraApp saludo={saludo}/>) del wrapper podemos sacar el getByText
    //     const {getByText} = render(<PrimeraApp saludo={saludo}/>);
    //     expect(getByText(saludo)).toBeInTheDocument();
    // });
    //trabajando con enzyne
    test('debe de mostrar <PrimeraApp /> correctamente', ()=>{
        const saludo = 'Hola, soy Goku';
        const wrapper = shallow(<PrimeraApp saludo={saludo}/>); // para que se renderice con los valores que queremos
        expect(wrapper).toMatchSnapshot();
    });

    test('debe de mostrar el subtitulo enviado por props', ()=>{
        const saludo = 'Hola soy un robot';
        const subTitulo = 'Soy un subtitulo';
        const wrapper = shallow(
        <PrimeraApp 
            saludo={saludo}
            subTitulo={subTitulo}
        />); // para que se renderice con los valores que queremos
        const textoParrafo = wrapper.find('p').text();//parecido con el query selector, busca un párrafo para id #id
        //console.log(textoParrafo);
        expect(textoParrafo).toBe(subTitulo);
    });

    
});
