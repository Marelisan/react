// De manera convencional

const activo = true;
// let mensaje = '';
// if(activo){
//     mensaje = 'Activo';
// }else{
//     mensaje = 'Inactivo';
// }

// Con el operando condicional ternario
//const mensaje = (activo) ? 'Activo':'Inactivo'
//const mensaje = (activo) ? 'Activo':'null'

// Forma corta de hacer un if
//const mensaje = (activo=== true) && 'Activo'; // imprime Activo
const mensaje = !activo && 'Activo'; // imprime false
console.log(mensaje);
