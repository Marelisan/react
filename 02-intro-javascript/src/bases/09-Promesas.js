import { getHeroeById } from "./bases/08-imp-exp";

// const promesa = new Promise((resolve, reject)=>{
//   setTimeout(()=>{
    
//     const p1 = getHeroeById(2);
//     //resolve(p1);// pasa a then el argumento heroe
//     reject('No se pudo encontrar el héroe');
//   },2000)
// });
// promesa.then((heroe)=>{
//   console.log('heroe', heroe);
// })
// .catch(err=>console.warn(err));
const getHeroeByIdAsync = (id)=> {
  return new Promise((resolve, reject)=>{
    setTimeout(()=>{
      const p1 = getHeroeById(id);
      if(p1)
        resolve(p1);// pasa a then el argumento heroe
      else
        reject('No se pudo encontrar el héroe');
        
      //reject('No se pudo encontrar el héroe');
    },2000)
  });
}
getHeroeByIdAsync(1)
  //.then (heroe => console.log('Heroe', heroe))
  .then (console.log) //pasa como primer argumento el p1 al console.log
  //.catch(err=>console.warn(err));
  .catch(console.warn) // manda el mensaje de error al console.warn


