// Arreglos en JS
// const arreglo = new Array(100); // no se recomienda, solo si indicamos los espacios que va a tener
// arreglo.push(1);
const arreglo = [1,2,3,4];
// arreglo.push(1); Se puede agregar así valores al array pero es mejor el spread, 
// arreglo.push(2); no es recomendable porque push modifica el objeto principal
// arreglo.push(3); 
// arreglo.push(4);
let arreglo2 = [...arreglo,5];
const arreglo3 = arreglo2.map( function(numero){
   return numero*2;
});

console.log(arreglo);
console.log(arreglo2);
console.log(arreglo3);