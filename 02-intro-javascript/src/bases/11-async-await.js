// const getImagenPromesa = ()=>{
//     const promesa = new Promise ((resolve,reject)=>{
//         resolve('https://paginadeprueba.com')
//     })
//     return promesa;
// }
// getImagenPromesa().then(console.log);

//Aún se puede simplificar más

// const getImagenPromesa = ()=>new Promise (resolve=>
//         resolve('https://paginadeprueba.com')
//     )

// getImagenPromesa().then(console.log);


//Utilizando Async, sin async esto es una función síncrona, poniendo 
//async se convierte en una promesa

// const getImagen = async() =>  {
//     return ('https://paginadeprueba.com')
// }

// getImagen().then(console.log);

//Async puede ir solo, pero el await siempre va de la mano con el async

//await nos ayuda a que podamos trabajar todo el código como si fuera síncrono

const getImagen = async() =>  {
    try {
    const apiKey = 'Rk9QkvQXANxOwytNieiLIFkxaqdwZrnP';
    const resp = await fetch (`https://api.giphy.com/v1/gifs/random?apiKey=${apiKey}`);
    //await hace que termine lo del fetch antes de seguir ejecutando código
    const {data} = await resp.json();
    const {url} = data.images.original;
    const img = document.createElement('img');
    img.src = url;
    document.body.append(img);
        
    } catch (error) {
        // manejo del error
    }
    
}
getImagen();

