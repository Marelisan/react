//Desestructuración de objetos
const persona={
    nombre: 'Tony',
    edad: 45,
    clave:'Ironman'
};
// console.log(persona.nombre);
// console.log(persona.edad);
// console.log(persona.clave);

//const{nombre,edad,clave}= persona;

// const retornaPersona= (usuario)=>{

//     const{edad,clave,nombre}= usuario;
//     console.log(edad,clave,nombre)
// }
// retornaPersona(persona);

const useContext= ({clave,nombre,edad,rango='capitán'})=>{

    //console.log(nombre,edad, rango)
    return{
        nombreClave: clave,
        anios: edad,
        latlng:{
            lat: 12.456,
            lng: -23.567
        }
    }
}
const {nombreClave,anios,latlng:{lat,lng}} = useContext(persona);
//cosnt{lat,lng}= latlng;
console.log(nombreClave,anios);
console.log(lat,lng);